from django.contrib import admin
from podcast.models import Show, Episode


@admin.register(Show)
class ShowAdmin(admin.ModelAdmin):
    pass


@admin.register(Episode)
class EpisodeAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'show', 'season', 'number', 'pub_date']
    list_filter = ['show', 'season', 'pub_date']
    search_fields = ['show', 'title', 'categories']
    fields = [
        'show',
        ('season', 'number'),
        ('title', 'slug'),
        'description',
        ('summary', 'type'),
        'pub_date',
        'audio_url',
        ('audio_type', 'audio_len')
    ]
