import re
import hashlib

from django.db import models
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.translation import gettext as _

from blog.utils import slug_generator


class Show(models.Model):
    TYPE_CHOICES = (
        ("episodic", _("Episodic")),
        ("serial", _("Serial")),
    )
    type = models.CharField(_("type"), max_length=255, choices=TYPE_CHOICES, default="episodic")
    title = models.CharField(_("Title"), max_length=255)
    slug = models.SlugField(_("Slug"), unique=True, blank=True)
    description = models.TextField(_("Description"), max_length=3500)
    subtitle = models.CharField(_("Subtitle"), max_length=255)
    image = models.ImageField(_("Image"), upload_to="podcast")
    author_name = models.CharField(_("Author Name"), max_length=255)
    author_email = models.EmailField(_("Author Email"))
    categories = models.CharField(_("Categories"), max_length=2000)

    class Meta:
        verbose_name = _("Show")
        verbose_name_plural = _("Shows")

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('podcast:feed', args=[str(self.slug)])

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slug_generator(self)
        super(Show, self).save(*args, **kwargs)

    @classmethod
    def get_object(cls):
        obj, created = cls.objects.get_or_create(pk=1)
        return obj

    def get_categories(self):
        return re.split(', ?', self.categories)


class Episode(models.Model):
    TYPE_CHOICES = (
        ("full", _("Full")),
        ("trailer", _("Trailer")),
        ("bonus", _("Bonus")),
    )
    FILE_TYPE_CHOICES = (
        ("audio/mpeg", _("MP3 audio")),
        ("audio/x-m4a", _("M4A audio")),
        ("audio/ogg", _("OGG Audio")),
    )
    show = models.ForeignKey(Show, verbose_name=_("Show"), on_delete=models.CASCADE)
    title = models.CharField(_("Title"), max_length=255)
    slug = models.SlugField(_("Slug"), blank=True, unique=True)
    description = models.TextField(_("Description"))
    pub_date = models.DateTimeField(_("Pub Date"))
    type = models.CharField(_("Type"), max_length=255, choices=TYPE_CHOICES, default="full")
    season = models.PositiveIntegerField(_("Season"), null=True)
    number = models.PositiveIntegerField(_("Number"), null=True, blank=True)
    summary = models.CharField(_("Summary"), max_length=255, blank=True)
    guid = models.CharField(_("GUID"), max_length=64, editable=False)
    audio_url = models.URLField(_("Audio URL"))
    audio_type = models.CharField(_("Audio Type"), max_length=255, choices=FILE_TYPE_CHOICES, default="audio/ogg")
    audio_len = models.PositiveIntegerField(_("Audio Length"), default=0)

    class Meta:
        verbose_name = _("Episode")
        verbose_name_plural = _("Episodes")
        ordering = ['-season', '-number']

    def __str__(self):
        return "%s - %s" % (self.number, self.title)

    def save(self, *args, **kwargs):
        super(Episode, self).save(*args, **kwargs)
        if not self.slug:
            self.slug = slug_generator(self)
        if not self.guid:
            bytes_id = force_bytes(self.id)
            self.guid = hashlib.sha256(bytes_id).hexdigest()
            self.save(update_fields=["guid"])

    def get_title(self):
        return "%s %s:%s - %s" % (self.show.title, _("Episode"), self.number, self.title)

    def get_author_name(self):
        return self.show.author_name

    def get_author_email(self):
        return self.show.author_email
