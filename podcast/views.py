from django.contrib.sites.shortcuts import get_current_site
from django.contrib.syndication.views import Feed, add_domain
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.utils.feedgenerator import Rss201rev2Feed, rfc2822_date

from podcast.models import Show, Episode


class ItunesFeed(Rss201rev2Feed):
    def rss_attributes(self):
        attrs = super(ItunesFeed, self).rss_attributes()
        attrs['xmlns:itunes'] = 'http://www.itunes.com/dtds/podcast-1.0.dtd'
        attrs['xmlns:content'] = 'http://purl.org/rss/1.0/modules/content/'
        return attrs

    def add_root_elements(self, handler):
        super(ItunesFeed, self).add_root_elements(handler)
        handler.addQuickElement('copyright', self.feed['copyright'])
        handler.addQuickElement('docs', self.feed['docs'])
        handler.startElement('image', {})
        handler.addQuickElement('url', self.feed['image'])
        handler.addQuickElement('title', self.feed['title'])
        handler.addQuickElement('link', self.feed['link'])
        handler.endElement('image')
        handler.addQuickElement('managingEditor', self.feed['managing_editor'])
        handler.addQuickElement('pubDate', rfc2822_date(self.latest_post_date()))
        handler.addQuickElement('webMaster', self.feed['webmaster'])
        handler.addQuickElement('itunes:type', self.feed['itunes']['type'])
        handler.addQuickElement('itunes:subtitle', self.feed['itunes']['subtitle'])
        handler.addQuickElement('itunes:summary', self.feed['itunes']['summary'])
        handler.addQuickElement('itunes:author', self.feed['itunes']['author']['name'])
        handler.startElement('itunes:owner', {})
        handler.addQuickElement('itunes:name', self.feed['itunes']['owner']['name'])
        handler.addQuickElement('itunes:email', self.feed['itunes']['owner']['email'])
        handler.endElement('itunes:owner')
        handler.addQuickElement('itunes:image', '', {'href': self.feed['image']})

    def add_item_elements(self, handler, item):
        super(ItunesFeed, self).add_item_elements(handler, item)
        handler.addQuickElement('itunes:episodeType', item['itunes']['type'])
        handler.addQuickElement('itunes:title', item['itunes']['title'])
        handler.addQuickElement('itunes:summary', item['itunes']['summary'])
        handler.addQuickElement('itunes:author', item['itunes']['author']['name'])
        handler.addQuickElement('itunes:image', '', {'href': item['itunes']['image']})


class ShowFeed(Feed):
    feed_type = ItunesFeed
    docs = 'http://cyber.harvard.edu/rss/rss.html'
    item_guid_is_permalink = False

    def get_object(self, request, slug):
        self.request = request
        return get_object_or_404(Show, slug=slug)

    def title(self, obj):
        return '%s' % obj.title

    def link(self, obj):
        return obj.get_absolute_url()

    def description(self, obj):
        return obj.description

    def categories(self, obj):
        return [c for c in obj.get_categories()]

    def ttl(self, obj):
        return 60

    def copyright(self, obj):
        year = timezone.now().year
        title = obj.title
        return '&#x2117; &amp; &#xA9; %s %s' % (year, title)

    def image(self, obj):
        current_site = get_current_site(self.request)
        return add_domain(current_site.domain, obj.image.url, self.request.is_secure())

    def feed_extra_kwargs(self, obj):
        return {
            'docs': self.docs,
            'image': self.image(obj),
            'copyright': self.copyright(obj),
            'managing_editor': obj.author_email,
            'webmaster': obj.author_email,
            'itunes': {
                'type': obj.type,
                'subtitle': obj.subtitle,
                'summary': obj.subtitle,
                'author': {
                    'name': obj.author_name,
                    'email': obj.author_email,
                },
                'owner': {
                    'name': obj.author_name,
                    'email': obj.author_email,
                },
                'categories': obj.get_categories(),
            }
        }

    def items(self, obj):
        return Episode.objects.filter(show=obj)

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.description

    def item_guid(self, item):
        return item.guid

    def item_pubdate(self, item):
        return item.pub_date

    def item_enclosure_url(self, item):
        return item.audio_url

    def item_enclosure_length(self, item):
        return item.audio_len

    def item_enclosure_mime_type(self, item):
        return item.audio_type

    def item_image(self, item):
        current_site = get_current_site(self.request)
        return add_domain(current_site.domain, item.show.image.url, self.request.is_secure())

    def item_link(self, item):
        return item.audio_url

    def item_extra_kwargs(self, item):
        return {
            'itunes': {
                'type': item.type,
                'number': item.number,
                'title': item.title,
                'summary': item.summary,
                'author': {
                    'name': item.get_author_name(),
                    'email': item.get_author_email(),
                },
                'image': self.item_image(item),
            }
        }
