
from django.urls import path

from podcast import views

app_name = 'podcast'

urlpatterns = [
    path('<slug:slug>', views.ShowFeed(), name="feed"),
]
