## Nedir Bu Proje?
* **[Canlı Örnek](http://kofteistan.com/)**
* [Özellikleri neler?](#özellikleri-neler)
* [Neden Markdown?](#neden-markdown)
* [Neden bu kadar sade?](#neden-bu-kadar-sade)
* [Forklanabilirlik!!!](#forklanabilirlik)
* [Yapılacaklar listesi:](#yapılacaklar-listesi)

Bu proje [Köfteist Köfte](http://kofteistan.com) yani benim kişisel sitem için yazdığım basit bir Django projesidir.
Projenin temel amacı aşırı sade ve minimal bir blog sitesi olmaktır ve bu amaçla mümkün olduğunca gereksiz içerikten kaçınılmıştır.

## Özellikleri neler?
Benim birkaç temel ihtiyacım olan ögeden oluşan aşırı sade bir listeye sahiptir:

* Blog: en temel özelliği olarak blog olması gereken bir özellik. Diğer türlü site direkt ana amacında başarısız olmuştu.
* Markdown editör: Site içerisindeki bütün metin alanları [Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) destekli olarak hazırlanmıştır. Dolayısıyla da Reddit, Gitlab, Github gibi yerlerde metinlerde yapabildiğiniz her şeyi rahatlıkla burada da yapabilirsiniz.
* Yorumlar: Çok karmaşık olmayan ama temel işlerin hepsini, hatta yanıt özelliğini bile destekleyen bir yorum altyapısı söz konusu. Sitedeki bütün yazı alanları gibi bu da Markdown destekli
* Gravatar: Yorum yapan kullanıcıların eposta adresleri aracılığıyla Gravatar profillerinden avatarlarını çekerek site içerisinde avatar gösterebiliyor.
* Etiketler üzerinden filtreleme: Arama ya da kategori özellikleri benim için çok gerekli olmayan özellikler olduğundan onların yerine site içerisinde rahat gezilmesi için bir etiket sistemi yerleştirdim.
* Podcast Feed çıktısı: [Youtube Kanalımda](https://youtube.com/user/kofteistkofte/) yayınladığım podcast için eklediğim ufak bir RSS feed.
* Minimal HTML ve CSS: Sitenin CSS ve HTML dosyalarını mümkün olduğunca gereksiz içerikten temiz halde yapmak önemli bir amaçtı. Bunu büyük ölçüde başardığımı düşünüyorum.
* Eser düzeyde JS: Bu site içerisinde amacım hiçbir şekilde JS kullanmamaktı, ancak yorumlara "Yanıtla" özelliği eklemek istediğimden bu maalesef olamadı. Ama en azından sadece tek bir JS fonksiyonu barındırması da benim için bir başarı. Eğer bunu JS olmadan da yaptırabilecek bir yol bulursanız gönül rahatlığıyla commit atabilirsiniz.
* Rahat okunabilir, kullanılabilir ve erişilebilir düzen: Sitenin her şeyden önce en büyük amacı tamamen okunabilirlik, kolay kullanım ve kolay erişim. Bu amaçla diğer kararların çoğu verildi.

## Neden Markdown?
Editör seçimimi Markdown'dan yana yapmamın sebebi tamamen temiz bir HTML çıktısı almak istemem ve işim gereği hali hazırda Markdown kullanıyor olmamdı.
Bunun yanı sıra üç önemli problemi de beraberinde çözüş oldu. Hem siteye çok yük bindirecek bir editör kurmadan yazı içeriğini istediğim gibi düzenleyebiliyor, yorum formu aşırı minimal olduğu halde bilen biri için aşırı güçlü bir halde, hem de bir ara eklemeyi düşündüğüm bir özellik için güzel bir altyapı sağlıyor.

## Neden bu kadar sade?
Bu sitenin yapılışında iki önemli amaç vardı. İlki sitenin terminal tabanlı tarayıcılarda bile sorunsuz çalışabilmesini istemem. Diğer ve en önemli sebep ise sitenin aşırı kolay okunabilir olması. Bu yüzden sitenin okunabilirliği düşünülerek hazırlandı her şey. Yani geri kalan hiçbir şey önemli değildi...

## Forklanabilirlik!!!
Sitede her şey aşırı minimumda tutulduğum için eğer bir blog sitesine ihtiyaç duyuyorsanız ihityaç duyacağınız en temel ihtiyaçların hepsi burada olup üzerine her senaryoya uymayabilecek özellikler neredeyse hiç eklenmediğim (Podcast Feed'i hariç) için daha detaylı herhangi bir proje için güzel bir temel olarak kullanılabilir. Gönül rahatlığıyla forklayıp kendi projenizin bir parçası olarak kullanabilirsiniz. Hatta bir hata, ya da herkesin kullanabileceğini düşündüğünüz bir özellik varsa eğer her türlü katkıya açığım.

## Yapılacaklar listesi:
Aklıma olan ve bir ara eklemeyi düşündüğüm ama sitenin çalışması için zorunlu olmayan özellikler:
* Markdown import: Basit bir form üzerinden bir .md dosyası yüklenerek siteye hızlıca yazı girilmesini sağlamak. Yazıları online olmadan da herhangi bir yerde yazıp sadece siteye bir dosya yükleyerek yazı yayınlamak için güzel bir yöntem olacağı kanısındayım.
* Basit bir yönetim arayüzü: Django admin paneli pek çok senaryo için harika bir araç, ama gene de bu projenin konsepti için uygun olmadığı kanısındayım. Sitenin yapısına daha uygun, sadece yazıları, yorumları, sayfaları yönetebilmeme, yeni yazı ve sayfa yüklememe izin verecek basit bir yönetici paneli yazıp Django Admin'i devre dışı bırakkmayı düşünüyorum.
* **Tartışmalı** / Sayfaya özel CSS: Şu an her ne kadar style.css dosyası aşırı minimal olsa da daha da minimal olabileceği kanısındayım. Planladığım şey bir sayfa ile beraber sadece o sayfada yüklenecek CSS kuralları yüklenecek. Siteyi daha mı hızlı yapar yoksa daha mı yavaş yapar emin değilim. O yüzden yapmayabilirim.
* Düzenli sadeleştirme: HTML ve CSS dosyalarını eğer yapabilirsem biraz daha minimize etmek.
