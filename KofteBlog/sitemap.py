from django.urls import reverse
from django.contrib.sitemaps import Sitemap

from blog.models import Page
from blog.views import post_queryset


class StaticViewSitemap(Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return ['blog:index']

    def location(self, item):
        return reverse(item)


class PageSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return Page.objects.all()


class PostSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return post_queryset()
