from django.urls import path, include
from django.conf import settings
from django.contrib import admin
from django.views.generic import TemplateView
from django.conf.urls.static import static
from django.contrib.sitemaps.views import sitemap

from .md_view import markdown_uploader
from .sitemap import StaticViewSitemap, PostSitemap

sitemaps = {
    'static': StaticViewSitemap,
    'post': PostSitemap,
}

urlpatterns = [
    path('podcast/', include('podcast.urls')),
    path('admin/', admin.site.urls),
    path('martor/', include('martor.urls')),
    path('api/uploader/', markdown_uploader, name='markdown_uploader_page'),
    path('panel/', include('panel.urls')),
    path('', include('blog.urls')),
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
    path('robots.txt', TemplateView.as_view(template_name="robots.txt", content_type="text/plain")),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
