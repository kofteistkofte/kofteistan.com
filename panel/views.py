from django.views import generic
from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin

from panel.utils import get_meta, get_desc
from panel.forms import MDUploadForm
from panel.forms import MDUploadForm, PostForm
from blog.models import Post


class MDUploadView(LoginRequiredMixin, generic.FormView):
    template_name = "panel/md_upload.html"
    form_class = MDUploadForm

    def post(self, request, *args, **kwargs):
        form = MDUploadForm(request.POST, request.FILES)
        if form.is_valid():
            raw_text = (request.FILES['file'].read()).decode()
            md_meta = get_meta(raw_text)
            post = Post(
                title=md_meta['title'][0],
                keywords=md_meta['keywords'][0],
                body=raw_text,
                thumb=request.FILES['thumb']
            )
            try:
                post.desc = md_meta['description'][0]
            except Exception:
                post.desc = get_desc(raw_text, md_meta)
            post.save()
        return redirect('blog:detail', post.url)


class PostUpdateView(LoginRequiredMixin, generic.edit.UpdateView):
    model = Post
    template_name = "panel/edit.html"
    slug_field = "url"
    form_class = PostForm
