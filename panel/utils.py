import markdown


def get_meta(*args):
    md = markdown.Markdown(extensions=['markdown.extensions.meta'])
    md.convert(args[0])
    return md.Meta


def get_desc(*args):
    raw_text = args[0]
    lines = raw_text.splitlines()
    desc = ""
    lnc = 0
    for line in lines[len(args[1])+2:]:
        if line is not "":
            desc += line
            lnc += len(line)
            if lnc >= 700 and lnc <= 800:
                break
            elif lnc > 800:
                desc = desc[:800] + "..."
                break
            else:
                desc += "\n"
    return desc
