from django import forms
from django.utils.translation import gettext as _

from blog.models import Post


class MDUploadForm(forms.Form):
    file = forms.FileField(label=_("Markdown File"))
    thumb = forms.FileField(label=_("Thumbnail"))


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        exclude = ['hit_count']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['body'].widget = forms.widgets.Textarea(attrs={'cols':100, 'rows':30})
        self.fields['pub_time'].widget = forms.widgets.DateTimeInput()
