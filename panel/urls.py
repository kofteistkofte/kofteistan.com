from django.urls import path

from . import views

app_name = 'panel'

urlpatterns = [
    path('md_upload/', views.MDUploadView.as_view(), name="md_upload"),
    path('edit/post/<slug:slug>/', views.PostUpdateView.as_view(), name="post_edit"),
]
