��    X      �     �      �  $   �  	   �     �     �     �     �  
     	             $     0  
   B     M     R  
   X     c     k     t  .   |     �     �     �     �     �     �     �     	  +   %	     Q	     V	     [	     h	  	   v	     �	     �	     �	     �	     �	  	   �	  	   �	     �	     �	  !   �	     
     
  	   
     $
     *
     /
     5
  v   <
  F   �
     �
     �
                    $     :  	   C  	   M     W     ]     y     ~     �     �     �     �     �     �  *   �  ,   �  	          -   "     P     X     ]     z     ~     �     �     �  4   �  2   �       "       A     a     h  "   o  	   �     �  
   �     �     �     �     �                         '     -     6  =   =     {  
   �     �     �  
   �     �     �  &   �  4        :     >     C     Q     b     u     }  	   �     �  
   �     �     �     �     �  $   �               #     +     1     7     @  �   H  p   �     [     d     p     u     �     �     �     �     �     �     �     �     �                          -     :  ;   K  7   �     �     �  9   �     
          !     >     D     P     n     �  .   �  ;   �     �     E   T          N   I       #           "   A   K   5   @   /   X       (      D           :           2         9             F   -       S   %   6   G       >           ?   H   L   W   4          0   8      3          1      B           <   !       =               Q                     J          .   M                  V                ,       ;       	           O           &              7      $   *   R   U      )         P   +       
                  '      C        %(count)s Comment %(count)s Comments Anonymous Answer Answer for the question above Answers Audio Length Audio Type Audio URL Author Email Author Name Bad image format. Blog Posts Body Bonus Categories Comment Comments Correct Could be worst, you could get Error 418 too... Count Description Draft Episode Episodes Error 404: Page Not Found Error 500: Server Error Feel FREE to fork it. %(icon)s For seperate answers, please use new lines. Full GUID Given Answer Given Answers Hit Count Image Invalid request! Keywords Leave a Comment Links M4A audio MP3 audio Markdown Cheatsheat Markdown File Maximum image file is %(size) MB. Next Number OGG Audio Order Page Pages Parent Please do not fill this fields if you think you are a human or any other sentient being. This is just a trap for bots. Please send an issue to me with every step you took to get this error. Post Posts Previous Pub Date Publish Time Put your comment here Question Questions Read More Reply Results for keyword "%(q)s" Send Show Shows Slug Submit Comment Submit Time Subtitle Summary The page you're looking for does not exist There is no comment yet. Wanna be the first? Thumbnail Title To write a comment please answer the question Trailer Type Ups, something went wrong... Url User User's email address User's name Wrong Answer You can see the codes of this website at %(gitlab)s. You can use Markdown to give style to your comment type Project-Id-Version: 1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-10-25 18:53+0300
Last-Translator: Göktuğ Korkmaz <goktug@bypassdigital.com>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 %(count)s Yorum %(count)s Yorum Anonim Yanıt Yukarıdaki soruyu yanıtlayınız Yanıtlar Dosya Uzunluğu Dosya Tipi Dosya Adresi Yayıncı Epostası Yayıncı Adı Geçersiz görsel formatı Gönderiler Gövde Bonus Kategoriler Yorum Yorumlar Doğru Daha kötüsü de olabilirdi, 418 hatası da alabilirdiniz... Sayısı Açıklama Taslak Bölüm Bölümler Hata 404: Sayfa Bulunamadı Hata 500: Sunucu Hatası ÖZGÜRCE forklayabilirsiniz. %(icon)s Farklı yanıtları yeni satır oluşturarak giriniz Tam GUID Verilen Cevap Verilen Cevaplar Tıklanma Sayısı Görsel Geçersiz istek! Etiketler Bir Yorum Bırak Adreslerim M4A Ses MP3 Ses Markdown Rehberi Markdown Dosyası En büyük görsel boyutu %(size) MB Eski Bölüm Numarası OGG Ses Sıra Sayfa Sayfalar Evebeyn Eğer İNSAN ya da herhangi bir bilinç sahibi varlık olduğunuzu düşünüyorsanız Lütfen bu alanı doldurmayın. Bu alan sadece botlar için bir tuzaktır. Lütfen bana bu hatayı almak için attığınız bütün adımları da anlatarak bir hata bildiriminde bulunun. Gönderi Gönderiler Yeni Yayın Tarihi Yayın Zamanı Yorumunuzu buraya yazınız Soru Sorular Devamını Oku Yanıtla "%(q)s" etiketi için sonuçlar Gönder Seri Seriler Slug Yorumu Gönder Gönderi Zamanı Alt başlık Kısa Açıklama Aradığınız sayfa artık aramızda yok, ya da hiç yoktu Henüz bir yorum yapılmamış. İlk olmak ister misin? Görsel Başlık Bir yorum yazabilmek için lütfen soruyu yanıtlayınız Fragman Bölüm Türü Bir şeyler doğru değil... Adres Kullanıcı Kullanıcının Eposta Adresi Kullanıcının Adı Yanlış Cevap Bu sitenin kodları %(gitlab)s içerisindedir. Yorumunuzu şekillendirmek için Markdown kullanabilirsiniz Tür 