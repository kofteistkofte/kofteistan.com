import re

from django.db import models
from django.conf import settings
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext as _

from martor.models import MartorField

from .utils import slug_generator, get_gravatar


class ContentAbstractModel(models.Model):
    """
    Abstract class for contents. It contains must have fields and functions for a content.
    """
    title = models.CharField(_("Title"), max_length=200)
    url = models.SlugField(_("Url"), max_length=200, blank=True)
    desc = models.TextField(_("Description"), max_length=1000, null=True)
    keywords = models.CharField(_("Keywords"), max_length=1000, null=True, blank=True)
    body = MartorField(_("Body"))
    thumb = models.ImageField(_("Thumbnail"), upload_to="thumbs/%Y/%m", null=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.title

    def get_tag_list(self):
        """
        A basic regular expression for converting keywords field to a list.
        """
        if self.keywords:
            return re.split(', ?', self.keywords)

    def save(self, *args, **kwargs):
        # Checks if there is a slug, if not calls slug_generator.
        if not self.url:
            self.url = slug_generator(self)
        super(ContentAbstractModel, self).save(*args, **kwargs)


class Page(ContentAbstractModel):
    order = models.PositiveSmallIntegerField(_("Order"), unique=True)

    class Meta:
        verbose_name = _("Page")
        verbose_name_plural = _("Pages")
        ordering = ['order']

    def get_absolute_url(self):
        return reverse('blog:page', args=[str(self.url)])


class Post(ContentAbstractModel):
    pub_time = models.DateTimeField(_("Publish Time"), null=True, blank=True)
    draft = models.BooleanField(_("Draft"), default=True)
    hit_count = models.PositiveIntegerField(_("Hit Count"), default=0)

    class Meta:
        verbose_name = _("Post")
        verbose_name_plural = _("Posts")
        ordering = ['-pub_time']

    def get_absolute_url(self):
        return reverse('blog:detail', args=[str(self.url)])

    def add_hit(self):
        """ The most ugly, yet still functional hit counter """
        self.hit_count += 1
        self.save()

    def save(self, *args, **kwargs):
        # Checks if post is draft and has publish time info. If both are not, fills with current time.
        if not self.draft and not self.pub_time:
            self.pub_time = timezone.now()
        super(Post, self).save(*args, **kwargs)


class Comment(models.Model):
    post = models.ForeignKey(Post, related_name="comments", verbose_name=_("Post"), on_delete=models.CASCADE)
    parent = models.ForeignKey("self", related_name="child", verbose_name=_("Parent"),
                               on_delete=models.SET_NULL, blank=True, null=True,
                               limit_choices_to={'parent': None})
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('User'),
                             blank=True, null=True, related_name="comments",
                             on_delete=models.SET_NULL)
    user_name = models.CharField(_("User's name"), max_length=50, null=True, blank=True)
    user_email = models.EmailField(_("User's email address"), null=True, blank=True)
    sub_time = models.DateTimeField(_('Submit Time'), default=None, db_index=True, blank=True)
    comment = models.TextField(_("Comment"))

    class Meta:
        verbose_name = _("Comment")
        verbose_name_plural = _("Comments")
        ordering = ['sub_time']

    def __str__(self):
        return "%s: %s..." % (self.user_name, self.comment[:50])

    def get_avatar(self):
        return get_gravatar(self.user_email)

    def save(self, *args, **kwargs):
        if self.sub_time is None:
            self.sub_time = timezone.now()
        if self.user:
            self.user_name = self.user.get_full_name()
            self.user_email = self.user.email
        else:
            if self.user_name is None:
                self.user_name = _("Anonymous")
        super(Comment, self).save(*args, **kwargs)


class Question(models.Model):
    question = models.CharField(_("Question"), max_length=200)
    answers = models.TextField(_("Answers"), help_text=_("For seperate answers, please use new lines."))

    class Meta:
        verbose_name = _("Question")
        verbose_name_plural = _("Questions")

    def __str__(self):
        return self.question

    def check_answer(self, **kwargs):
        """
        A simple function to create a list from object's answers and checks if given one is in it.
        To use it, call it as **object**.check_answer(answer=**keyword**)
        """
        if kwargs['answer'] in self.answers.splitlines():
            return True


class GivenAnswer(models.Model):
    question = models.ForeignKey(Question, verbose_name=_("Question"), on_delete=models.CASCADE)
    answer = models.CharField(_("Answer"), max_length=200)
    count = models.PositiveSmallIntegerField(_("Count"), default=0)
    correct = models.BooleanField(_("Correct"), default=False)

    class Meta:
        verbose_name = _("Given Answer")
        verbose_name_plural = _("Given Answers")

    def __str__(self):
        return "%s - %s" % (self.answer, self.question.__str__())

    def save(self, *args, **kwargs):
        # Adds one to objects count everytime it got updated.
        self.count = self.count + 1
        super(GivenAnswer, self).save(*args, **kwargs)
