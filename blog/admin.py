from django.contrib import admin
from .models import Post, Page, Comment, Question, GivenAnswer


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ['title', 'url', 'pub_time', 'hit_count', 'draft']
    list_editable = ['draft']
    list_filter = ['pub_time', 'draft']
    search_fields = ['title', 'keywords', 'desc', 'body']
    fields = [
        ('title', 'url'),
        ('keywords', 'draft'),
        'desc',
        'thumb',
        'pub_time',
        'body',
    ]


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    list_display = ['title', 'url', 'order']
    list_editable = ['order']
    fields = [
        ('title', 'url'),
        ('keywords', 'order'),
        'desc',
        'thumb',
        'body',
    ]


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'post', 'sub_time']
    list_filter = ['sub_time', 'post']
    search_fields = ['post', 'user_name', 'user_email', 'parent', 'comment']
    fields = [
        ('post', 'user'),
        ('user_name', 'user_email'),
        'parent',
        'sub_time',
        'comment',
    ]


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    pass


@admin.register(GivenAnswer)
class GivenAnswerAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'correct', 'count']
    list_filter = ['correct', 'question__question']
    search_fields = ['answer', 'question__question']
