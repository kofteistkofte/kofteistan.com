from django import template
from django.utils.safestring import mark_safe

from ..utils import markdownify

register = template.Library()


@register.filter
def render_md(field_name):
    return mark_safe(markdownify(field_name))
