from django.views import generic
from django.utils import timezone
from django.shortcuts import redirect
from django.contrib.syndication.views import Feed
from django.utils.translation import gettext as _

from .models import Post, Page, Question, GivenAnswer
from .forms import CommentForm, UserCommentForm


def post_queryset():
    return Post.objects.filter(draft=False)


class IndexView(generic.ListView):
    template_name = "index.html"
    context_object_name = "post_list"
    paginate_by = 12

    def get_queryset(self):
        return post_queryset()


class PageView(generic.DetailView):
    model = Page
    template_name = "detail.html"
    slug_field = "url"
    context_object_name = "post"


class PostDetailView(PageView):
    model = Post

    def get_context_data(self, *args, **kwargs):
        self.object.add_hit()  # Calling ugly hit counter
        context = super(PostDetailView, self).get_context_data(*args, **kwargs)
        context['question'] = Question.objects.order_by('?').first()
        # Error handling for comment form
        try:
            err = self.request.GET.get('err')
            if err == "wrong_answer":
                context['com_err'] = _("Wrong Answer")
        except Exception:
            pass
        if self.request.user.is_authenticated:
            form = UserCommentForm()
        else:
            form = CommentForm()
        context['form'] = form
        return context

    def post(self, request, *args, **kwargs):
        self.object = post = self.get_object()
        # If page is viewed by a logged in user, renders UsercommentForm, if not, CommentForm
        if self.request.user.is_authenticated:
            form = UserCommentForm(request.POST)
        else:
            form = CommentForm(request.POST)

        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = post
            # If post made by a logged in user, fills user, user_name and user_email fields automatically
            if self.request.user.is_authenticated:
                comment.user = request.user
                comment.user_name = request.user.get_full_name()
                comment.user_email = request.user.email
            comment.sub_time = timezone.now()
            # Getting question and it's anwer
            question = Question.objects.get(pk=form.cleaned_data.get('question'))
            answer = form.cleaned_data.get('answer').lower()
            if question.check_answer(answer=answer) is True:
                comment.save()
                # Creating or updating an object for correct answers
                GivenAnswer.objects.update_or_create(
                    question=question,
                    answer=answer,
                    correct=True,
                )
            else:
                # Same on top but for false answers
                GivenAnswer.objects.update_or_create(
                    question=question,
                    answer=answer,
                )
                return redirect(post.get_absolute_url() + "?err=wrong_answer#comment_form")
            return redirect(post.get_absolute_url())
        else:
            context = self.get_context_data()
            context['error'] = True
            return self.render_to_response(context)


class TagListView(IndexView):
    """
    A view that actually searches asked tag in keywords field of posts.
    """
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["q"] = self.request.GET.get('q')
        return context

    def get_queryset(self):
        return post_queryset().filter(keywords__contains=self.request.GET.get('q'))


class SiteFeed(Feed):
    """
    A view for RSS feed.
    """
    title = _("Blog Posts")
    link = "/feed/"
    description_template = "partials/feed.html"

    def items(self):
        return post_queryset()[:28]
