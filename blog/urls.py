from django.urls import path

from . import views

app_name = 'blog'

urlpatterns = [
    path('', views.IndexView.as_view(), name="index"),
    path('feed/', views.SiteFeed(), name="feed"),
    path('post/<slug:slug>/', views.PostDetailView.as_view(), name="detail"),
    path('tag/', views.TagListView.as_view(), name="tag"),
    path('<slug:slug>/', views.PageView.as_view(), name="page"),
]
