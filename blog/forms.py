from django import forms
from .models import Comment
from django.utils.translation import gettext as _


class UserCommentForm(forms.ModelForm):
    fullname = forms.CharField()
    question = forms.IntegerField(widget=forms.widgets.HiddenInput())
    answer = forms.CharField()

    class Meta:
        model = Comment
        fields = ['parent', 'comment', 'answer']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['parent'].widget = forms.widgets.HiddenInput()
        self.fields['fullname'].required = False
        self.fields['comment'].widget.attrs.update({
            'placeholder': _('Put your comment here'),
            'class': 'textfield',
            'required': True,
        })
        self.fields['answer'].widget.attrs.update({
            'placeholder': _('Answer for the question above'),
            'required': True,
        })

    def is_valid(self):
        valid = super().is_valid()
        if not valid:
            return False
        if self.cleaned_data['fullname']:
            return False
        return True


class CommentForm(UserCommentForm):
    class Meta:
        model = Comment
        fields = ['parent', 'user_name', 'user_email', 'comment']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['user_name'].widget.attrs.update({
            'placeholder': _("User's name"), 'class': 'singleline', 'required': True
        })
        self.fields['user_email'].widget.attrs.update({
            'placeholder': _("User's email address"), 'class': 'singleline', 'required': True
        })
